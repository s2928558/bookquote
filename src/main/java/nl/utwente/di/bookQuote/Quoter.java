package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private HashMap<String, Double> map;
    public HashMap<String, Double> loadMap() {
        this.map = new HashMap<>();
        this.map.put("1", 10.0);
        this.map.put("2", 45.0);
        this.map.put("3", 20.0);
        this.map.put("4", 35.0);
        this.map.put("5", 50.0);
        return this.map;
    }

    public double getBookPrice(String isbn) {
        return loadMap().getOrDefault(isbn, 0.0);
    }
}
